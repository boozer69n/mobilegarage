package com.csc330.caffeinatedrobots.mobilegarage;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by blake on 10/28/15.
 */
public class Repair implements Parcelable{
    private int id;             // the id (in db) of this vehicle
    private String type;        // the id of the type of repair (sched/unsched)
    private String date;        // the date of the repair
    private String cost;        // the cost of the repair
    private int mileage;        // the mileage at time of repair
    private String description; // a description of the repair

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getType() { return type; }
    public void setType(String type) { this.type = type; }
    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }
    public String getCost() { return cost; }
    public void setCost(String cost) { this.cost = cost; }
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public int getMileage() { return mileage; }
    public void setMileage(int mileage) { this.mileage = mileage; }

    // default constructor with no arguments needed.
    public Repair() {}


    public static final Parcelable.Creator<Repair> CREATOR
            = new Parcelable.Creator<Repair>() {
        public Repair createFromParcel(Parcel in) {
            return new Repair(in);
        }

        public Repair[] newArray(int size) {
            return new Repair[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Dump the items of this object into a parcel
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(date);
        dest.writeString(type);
        dest.writeString(cost);
        dest.writeInt(mileage);
        dest.writeString(description);
    }

    /**
     * Build a new Category from a parcel object
     * @param p The constructing parcel.
     */
    public Repair(Parcel p) {
        id = p.readInt();
        date = p.readString();
        type = p.readString();
        cost = p.readString();
        mileage = p.readInt();
        description = p.readString();
    }

}
