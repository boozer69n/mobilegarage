package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Blake on 11/1/2015.
 */
public class MobileGarageDatabase {
    MobileGarageHelper helper;

    public MobileGarageDatabase(Context context) {
        helper = new MobileGarageHelper(context, MobileGarageHelper.DATABASE_NAME, null,
                MobileGarageHelper.DATABASE_VERSION);
    }

    // Called when you no longer need access to the database.
    public void closeDatabase() {
        helper.close();
    }

    public void removeCar(int id) {
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(MobileGarageHelper.CARS_TABLE,
                MobileGarageHelper.KEY_ID + "=" + id, null);
        db.delete(MobileGarageHelper.REPAIRS_TABLE,
                MobileGarageHelper.REPAIR_CAR_ID + "=" + id, null);
    }

    public void addCar(String make, String model, String year,
                            String mileage, String image) {
        // Create a new row of values to insert.
        ContentValues newValues = new ContentValues();
        // Assign values for each row.
        newValues.put(MobileGarageHelper.CAR_MAKE, make);
        newValues.put(MobileGarageHelper.CAR_MODEL, model);
        newValues.put(MobileGarageHelper.CAR_YEAR, year);
        newValues.put(MobileGarageHelper.CAR_MILEAGE, mileage);
        newValues.put(MobileGarageHelper.CAR_IMAGE, image);
        // Insert the row into your table
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(MobileGarageHelper.CARS_TABLE, null, newValues);
    }

    public void updateCar(int id, String make, String model, String year,
                          String mileage, String image) {
        // Create a new row of values to insert.
        ContentValues newValues = new ContentValues();
        // Assign values for each row.
        newValues.put(MobileGarageHelper.CAR_MAKE, make);
        newValues.put(MobileGarageHelper.CAR_MODEL, model);
        newValues.put(MobileGarageHelper.CAR_YEAR, year);
        newValues.put(MobileGarageHelper.CAR_MILEAGE, mileage);
        newValues.put(MobileGarageHelper.CAR_IMAGE, image);
        // Insert the row into your table
        String where = MobileGarageHelper.KEY_ID + "=" + id;
        String whereArgs[] = null;

        // Update the row with the specified index with the new values.
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(MobileGarageHelper.CARS_TABLE, newValues,
                where, whereArgs);
    }

    public ArrayList<Car> getCars() {
        Cursor cursor = getCarCursor();
        ArrayList<Car> results = new ArrayList<Car>();

        // Find the index to the column(s) being used.
        int INDEX_ID = cursor.getColumnIndexOrThrow(MobileGarageHelper.KEY_ID);
        int INDEX_MAKE = cursor.getColumnIndexOrThrow(MobileGarageHelper.CAR_MAKE);
        int INDEX_MODEL = cursor.getColumnIndexOrThrow(MobileGarageHelper.CAR_MODEL);
        int INDEX_YEAR = cursor.getColumnIndexOrThrow(MobileGarageHelper.CAR_YEAR);
        int INDEX_MILEAGE = cursor.getColumnIndexOrThrow(MobileGarageHelper.CAR_MILEAGE);
        int INDEX_IMAGE = cursor.getColumnIndexOrThrow(MobileGarageHelper.CAR_IMAGE);

        while (cursor.moveToNext()) {
            Car car = new Car();
            car.setId(cursor.getInt(INDEX_ID));
            car.setMake(cursor.getString(INDEX_MAKE));
            car.setModel(cursor.getString(INDEX_MODEL));
            car.setYear(cursor.getInt(INDEX_YEAR));
            car.setMileage(cursor.getInt(INDEX_MILEAGE));
            car.setImage(cursor.getString(INDEX_IMAGE));
            results.add(car);
        }
        // Close the Cursor when you've finished with it.
        cursor.close();

        return results;
    }

    private Cursor getCarCursor() {
        String[] result_columns = new String[]{
                MobileGarageHelper.KEY_ID,
                MobileGarageHelper.CAR_MAKE,
                MobileGarageHelper.CAR_MODEL,
                MobileGarageHelper.CAR_YEAR,
                MobileGarageHelper.CAR_MILEAGE,
                MobileGarageHelper.CAR_IMAGE
        };
        // Replace these with valid SQL statements as necessary.
        String where = null;
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.query(helper.CARS_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        return cursor;
    }


//############################################################################################
    //                   REPAIR


    public void removeRepair(int id) {
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(MobileGarageHelper.REPAIRS_TABLE,
                MobileGarageHelper.KEY_ID + "=" + id, null);
    }

    public void addRepair(int car_id, String type, String date,
                       String cost, String description) {
        // Create a new row of values to insert.
        ContentValues newValues = new ContentValues();
        // Assign values for each row.
        newValues.put(MobileGarageHelper.REPAIR_CAR_ID, car_id);
        newValues.put(MobileGarageHelper.REPAIR_TYPE, type);
        newValues.put(MobileGarageHelper.REPAIR_DATE, date);
        newValues.put(MobileGarageHelper.REPAIR_COST, cost);
        newValues.put(MobileGarageHelper.REPAIR_DESCRIPTION, description);
        // Insert the row into your table
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(MobileGarageHelper.REPAIRS_TABLE, null, newValues);
    }

    public void updateRepair(int id, String type, String date,
                             String cost, String description) {
        // Create a new row of values to insert.
        ContentValues newValues = new ContentValues();
        // Assign values for each row.
        newValues.put(MobileGarageHelper.REPAIR_TYPE, type);
        newValues.put(MobileGarageHelper.REPAIR_DATE, date);
        newValues.put(MobileGarageHelper.REPAIR_COST, cost);
        newValues.put(MobileGarageHelper.REPAIR_DESCRIPTION, description);
        // Insert the row into your table
        String where = MobileGarageHelper.KEY_ID + "=" + id;
        String whereArgs[] = null;

        // Update the row with the specified index with the new values.
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(MobileGarageHelper.REPAIRS_TABLE, newValues,
                where, whereArgs);
    }

    public ArrayList<Repair> getRepairs() {
        Cursor cursor = getRepairCursor();
        ArrayList<Repair> results = new ArrayList<Repair>();

        // Find the index to the column(s) being used.
        int INDEX_ID = cursor.getColumnIndexOrThrow(MobileGarageHelper.KEY_ID);
        int INDEX_TYPE = cursor.getColumnIndexOrThrow(MobileGarageHelper.REPAIR_TYPE);
        int INDEX_DATE = cursor.getColumnIndexOrThrow(MobileGarageHelper.REPAIR_DATE);
        int INDEX_COST = cursor.getColumnIndexOrThrow(MobileGarageHelper.REPAIR_COST);
        int INDEX_DESCRIPTION = cursor.getColumnIndexOrThrow(MobileGarageHelper.REPAIR_DESCRIPTION);

        while (cursor.moveToNext()) {
            Repair repair = new Repair();
            repair.setId(cursor.getInt(INDEX_ID));
            repair.setType(cursor.getString(INDEX_TYPE));
            repair.setDate(cursor.getString(INDEX_DATE));
            repair.setCost(cursor.getString(INDEX_COST));
            repair.setDescription(cursor.getString(INDEX_DESCRIPTION));
            results.add(repair);

        }
        // Close the Cursor when you've finished with it.
        cursor.close();

        return results;
    }

    private Cursor getRepairCursor() {
        String[] result_columns = new String[]{
                MobileGarageHelper.KEY_ID,
                MobileGarageHelper.REPAIR_TYPE,
                MobileGarageHelper.REPAIR_DATE,
                MobileGarageHelper.REPAIR_COST,
                MobileGarageHelper.REPAIR_DESCRIPTION
        };
        // Replace these with valid SQL statements as necessary.
        String where = null;
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;

        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.query(helper.REPAIRS_TABLE,
                result_columns, where,
                whereArgs, groupBy, having, order);
        return cursor;
    }

    /**
     * @author Blake Sutton
     * @filename MobileGarageDatabase.java
     * @email blasutto@uat.edu
     * <p/>
     * Created by blake on 11/1/15.
     * @description This file defines the database tables and columns for easy use.
     */
    public class MobileGarageHelper extends SQLiteOpenHelper {

        private static final int DATABASE_VERSION = 3; // SQL Statement to create a new database.
        private static final String DATABASE_NAME = "mobilegarage.db";
        private static final String CARS_TABLE = "cars";
        private static final String REPAIRS_TABLE = "repairs";
        private static final String KEY_ID = "id";

        // columns for category
        private static final String CAR_MAKE = "make";
        private static final String CAR_MODEL = "model";
        private static final String CAR_YEAR = "year";
        private static final String CAR_MILEAGE = "mileage";
        private static final String CAR_IMAGE = "image";

        private static final String CREATE_TABLE_CAR =
                "create table " + CARS_TABLE +
                        " (" + KEY_ID + " integer primary key autoincrement, " +
                        CAR_MAKE + " text not null, " +
                        CAR_MODEL + " text not null, " +
                        CAR_YEAR + " int not null, " +
                        CAR_MILEAGE + " int not null, " +
                        CAR_IMAGE + " text not null);";

        private static final String REPAIR_CAR_ID = "car_id";
        private static final String REPAIR_TYPE = "type";
        private static final String REPAIR_DATE = "date";
        private static final String REPAIR_COST = "cost";
        private static final String REPAIR_DESCRIPTION = "description";

        private static final String CREATE_TABLE_REPAIR =
                "create table " + REPAIRS_TABLE +
                        " (" + KEY_ID + " integer primary key autoincrement, " +
                        REPAIR_CAR_ID + " integer not null, " +
                        REPAIR_TYPE + " text not null, " +
                        REPAIR_DATE + " text not null, " +
                        REPAIR_COST + " text not null, " +
                        REPAIR_DESCRIPTION + " text not null);";


        public MobileGarageHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_CAR);
            db.execSQL(CREATE_TABLE_REPAIR);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
            // Log the version upgrade.
            Log.w("MobileGarageDB", "Upgrading from version" +
                    oldVersion + " to " +
                    newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS '" + CARS_TABLE + "'");
            db.execSQL("DROP TABLE IF EXISTS '" + REPAIRS_TABLE + "'");
            // Create a new one.
            onCreate(db);
        }
    }
}
