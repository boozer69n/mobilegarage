package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by blake on 10/28/15.
 */
public class RepairListActivity extends AppCompatActivity {

    private Repair selectedRepair;
    private View selectedRowView;
    private Bitmap selectedImage;
    private Car car = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddRepairActivity.class);
                intent.putExtra("car", car);
                startActivity(intent);
            }
        });

        ArrayList repairs = getListData();
        final ListView repair_list = (ListView) findViewById(R.id.layout_list);
        registerForContextMenu(repair_list);
        repair_list.setAdapter(new RepairListAdaptor(this, repairs));
        repair_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Repair repair = (Repair) repair_list.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), RepairPreviewActivity.class);
                intent.putExtra("car", car);
                intent.putExtra("repair", repair);
                startActivity(intent);
            }
        });
        // get the intent that started this action
        Intent intent = getIntent();
        // get the category passed into it if it exists we are editing
        if (intent.hasExtra("car")) {
            car = intent.getParcelableExtra("car");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Repair repair = (Repair)((ListView) findViewById(R.id.layout_list)).getItemAtPosition(info.position);
        switch (item.getItemId()) {
            case R.id.action_edit:
                // we send both items so that we can return to this category after the edit.
                Intent intent = new Intent(getApplicationContext(), AddRepairActivity.class);
                intent.putExtra("repair", repair);
                intent.putExtra("car", car);
                startActivity(intent);
                return true;
            case R.id.action_delete:
                removeRepair(repair.getId());
                Intent refresh = new Intent(getApplicationContext(), RepairListActivity.class);
                refresh.putExtra("car", car);
                startActivity(refresh);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
        ListView lv = (ListView) v;
        if(v.getId() == R.id.layout_list) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Repair repair = (Repair) lv.getItemAtPosition(acmi.position);
            String desc = repair.getDescription();
            if(desc.length() > 15) {
                desc = desc.substring(0,15) + "...";
            }
            menu.setHeaderTitle(desc);
        }
    }

    // remove a snippet category.
    private void removeRepair(int id) {
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        db.removeRepair(id);
    }

    // this will build from a database this is just for testing
    private ArrayList getListData() {
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        return db.getRepairs();
    }
}
