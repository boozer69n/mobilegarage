package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * File: CarListAdaptor.Java
 * @author Blake Sutton
 *
 * This class is designed to grab a list of repairs and display them as repair
 * objects in the ayout_list item on the main page.
 */
public class RepairListAdaptor extends BaseAdapter {
    private ArrayList<Repair> listData;
    private LayoutInflater layoutInflater;

    public RepairListAdaptor(Context context, ArrayList<Repair> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.repair_row, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.repair_title_textview);
            holder.description = (TextView) convertView.findViewById(R.id.repair_description_textview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(listData.get(position).getDate() + " " + listData.get(position).getType());
        holder.description.setText(listData.get(position).getDescription());
        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView description;
    }
}

